### Programmer la calculatrice.

## Fonctionnement de la calculatrice.

!!! info "Schéma de fonctionnement"
    Nous allons voir sur un schéma comment fonctionnera cette calculatrice :

    ```mermaid
    stateDiagram-v2
        direction LR
        1: Entrée du nombre en Shadok
        1 --> Calculatrice
        state Calculatrice {
            2 : Conversion Shadok vers décimal
            3 : Calcul  en décimal
            4 : Conversion décimal vers Shadok
            2 --> 3
            3 --> 4
        }

        5 : Affichage du nombre en Shadok
        Calculatrice --> 5
    ```
## Script Python implémentant la calculatice Shadok.
!!! info "Script de la calculatrice"
    Le script python ci dessous implémente la calculatrice en Python, il utilise la bibliothèque `Tkinter` pour générer l'interface graphique. 
    
    Certaines fonctions n'ont pas été implémentées (cf. Travail à réaliser, ci-dessous).

    ??? info "Script Python"
        ``` python linenums="1"
        # coding: utf-8
        # LFT 2019/20020
        # Auteur : Rasolonirina Herimanana Yowann
        # Groupe NSI : Groupe 1
        # Date : 04 septembre 2019

        from tkinter import * # import bibliothéque gaphique
        import tkinter.font as tkFont # import des polices de caractères.

        # Initialisation des variables
        resultat = "GA"
        resultat1 =""
        resultat2 = ""
        operation = 0

        # fonctions de calculs et de conversions

        def dectoshadok(n):
            """ conversion décimal vers shadok """
            pass

        def shadoktodec(chaine) :
            """ Conversion shadok vers décimal """
            pass

        def addshadok(chaine1,chaine2) :
            """ additionne deux chaines de caractère en shadok 
            renvoie le résultat en shadok"""
            pass

        def subshadok(chaine1,chaine2) :
            """ soustrait deux chaines de cararctères en shadok 
            renvoie le résultat en shadok"""
            pass

        def multishadok(chaine1,chaine2) :
            """ multiplie deux chaines de caractères en shadok 
            renvoie le résultat en shadok"""
            pass

        # fonction d'affichage
        def affich_update(valeur):
            """ Mise à jour de l'affichage"""
            global resultat
            if resultat == "GA" :
                return (valeur)
            else :
                return (resultat + " "+ valeur)

        def meu() :
            global resultat, champ_label
            if shadoktodec(resultat) > 65536 :
                resultat = "Impossible d'afficher un si grand nombre"
            else :
                resultat = affich_update("MEU")
            champ_label.config(text=resultat)

        def zo() :
            global resultat, champ_label
            if shadoktodec(resultat) > 65536 :
                resultat = "Impossible d'afficher un si grand nombre"
            else :
                resultat = affich_update("ZO")
            champ_label.config(text=resultat)

        def bu() :
            global resultat, champ_label
            if shadoktodec(resultat) > 65536 :
                resultat = "Impossible d'afficher un si grand nombre"
            else :
                resultat = affich_update("BU")
            champ_label.config(text=resultat)

        def ga() :
            global resultat, champ_label
            if shadoktodec(resultat) > 65536 :
                resultat = "Impossible d'afficher un si grand nombre"
            else :
                resultat = affich_update("GA")
            champ_label.config(text=resultat)

        def affich_cls():
            global resultat, champ_label
            resultat = "GA"
            champ_label.config(text=resultat)

        def affich_addshadok ():
            global resultat, resultat1, resultat2, champ_label, operation
            operation = 1
            if resultat1 == "" :
                resultat1 = resultat
                resultat = "GA"
            champ_label.config(text=resultat)

        def affich_subshadok () :
            global resultat, resultat1, resultat2, champ_label, operation
            operation = 2
            if resultat1 == "" :
                resultat1 = resultat
                resultat = "GA"
            champ_label.config(text=resultat)

        def affich_multishadok () :
            global resultat, resultat1, resultat2, champ_label, operation
            operation = 3
            if resultat1 == "" :
                resultat1 = resultat
                resultat = "GA"
            champ_label.config(text=resultat)

        def affich_egal() :
            global resultat, resultat1, resultat2, champ_label,operation
            if resultat2 =="" :
                resultat2 = resultat
            if operation == 1 :
                if  (shadoktodec(resultat1)+shadoktodec(resultat2)) > 65536 :
                    resultat = "Impossible à afficher, nombre trop grand"
                else :
                    resultat = addshadok(resultat1,resultat2)
                operation = 0
            elif operation == 2 :
                if (shadoktodec(resultat1)-shadoktodec(resultat2))<0 :
                    resultat = "Impossible d'afficher un nombre négatif"
                else :
                    resultat = subshadok(resultat1,resultat2)
                operation = 0
            elif operation == 3 :
                if  (shadoktodec(resultat1)*shadoktodec(resultat2)) > 65536 :
                    resultat = "Impossible à afficher, nombre trop grand"
                else :
                    resultat = multishadok(resultat1,resultat2)
            resultat1=""
            resultat2=""
            champ_label.config(text=resultat)

        # Création de l'interface graphique.
        fenetre = Tk()
        fenetre.geometry=("500x300")
        cadre_affichage = Frame(fenetre, width=300, height=100, borderwidth=1)
        cadre_chiffre = Frame(fenetre, width=400, height=100, borderwidth=1)
        cadre_operation = Frame(fenetre, width=200, height=100, borderwidth=1)
        cadre_affichage.pack(side="top")
        cadre_chiffre.pack(side="left")
        cadre_operation.pack(side="right")

        # Déclaration des polices de caractères utilisées
        helv36 = tkFont.Font(family='Helvetica', size=36, weight='bold')
        helv20 = tkFont.Font(family='Helvetica', size=20, weight='bold')
        helv12 = tkFont.Font(family='Helvetica', size=12, weight='bold')

        # Création du label d'affichage
        champ_label = Label(cadre_affichage, text=resultat, font=helv12, bg="black", fg="white" ,height=2, width=40)
        champ_label.pack()

        # Création des boutons de la calculatrice
        bouton_MEU = Button(cadre_chiffre, text="MEU", command=meu, padx=10, pady=15,font=helv36)
        bouton_ZO = Button(cadre_chiffre, text="ZO", command=zo, padx=25, pady=15, font=helv36)
        bouton_BU = Button(cadre_chiffre, text="BU", command=bu, padx=33, pady=15,font=helv36)
        bouton_GA = Button(cadre_chiffre, text="GA", command=ga, padx=25, pady=15, font=helv36)
        bouton_MEU.grid(column=0, row=0)
        bouton_ZO.grid(column=1, row = 0)
        bouton_BU.grid(column=0, row=1)
        bouton_GA.grid(column=1, row=1)

        # Création des bouttons opérations
        bouton_plus = Button(cadre_operation, text="+", command=affich_addshadok, padx=10, pady=5,font=helv20)
        bouton_moins = Button(cadre_operation, text="-", command=affich_subshadok, padx=14, pady=5,font=helv20)
        bouton_multi = Button(cadre_operation, text="x", command=affich_multishadok, padx=10, pady=5,font=helv20)
        bouton_egal = Button(cadre_operation, text="=", command=affich_egal, padx=10, pady=5,font=helv20)
        bouton_cls = Button(cadre_operation, text="cls", command=affich_cls, padx=10, pady=5,font=helv20)
        bouton_plus.pack()
        bouton_moins.pack()
        bouton_multi.pack()
        bouton_egal.pack()
        bouton_cls.pack()

        fenetre.mainloop()

        #Jeu d'essai
        print("Pass test1 --> ", shadoktodec("BU BU") == 5)
        print("Pass test2 --> ", shadoktodec("MEU ZO BU") == 57)
        print("Pass test3 --> ", dectoshadok(2457) == "ZO BU ZO BU ZO BU ")
        print("Pass test4 --> ", addshadok("BU BU","MEU GA GA") == 'MEU BU BU ')
        print("Pass test5 --> ", addshadok("GA","GA") == 'GA ')
        print("Pass test6 --> ", subshadok("MEU MEU ZO","BU BU ZO") == 'ZO ZO GA ')
        print("Pass test7 --> ", multishadok("MEU MEU ZO","MEU MEU MEU") == 'MEU MEU BU GA GA ZO')


        ```

## Travail à réaliser

!!! Danger "Les étapes de travail"
    - Pour ce travail,vous devez travail en local. Vous pouvez utiliser Thonny par exemple.
    <center>
    [Télécharger Thonny](https://thonny.org/){ .md-button .md-button--primary }
    </center>
    - Copier/Coller le script ci dessus dans l'interface de Thonny
    - Implémenter les fonctions manquantes en se référant à leur spécification:
        - `dectoshadok(n)`
        - `shadoktodec(chaine)`
        - `addshadok(chaine1,chaine2)`
        - `subshadok(chaine1,chaine2)`
        - `multishadok(chaine1,chaine2)`
    - Sauvegarder votre script, et le déposer sur labo-sciences
    <center>
        [labo-sciences](https://labo-sciences.fr/moodle/){ .md-button .md-button--primary }
    </center>

    ??? Exemple "Un exemple de fonctionnement de la calculatrice shadok"
        <center>
        <video controls>
        <source src="calculatrice.mp4" codecs="avc1, 42E01E,mp4a,40.2" type="video/mp4" />
        </video>
        </center>
