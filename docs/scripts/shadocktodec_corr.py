def shadoktodec(chaine):
    """ Cette fonction prend en argument une chaîne de 
    caractères shadoks ('BU GA ZO') par exemple, et renvoie l'entier correspondant"""
    # split() permet de diviser une chaîne de caractères à partir d'un séparateur 
    chaine_traitee = chaine.split(" ")
    resultat = 0
    rang = 0
    longueur = len(chaine_traitee)
    for i in chaine_traitee :
        resultat = resultat + conv(i)*4**(longueur-rang-1)
        rang += 1
    return(resultat)