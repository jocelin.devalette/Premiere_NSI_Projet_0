def dectoshadok(n):
    """ conversion décimal vers shadok """
    shadok =["GA" , "BU" , "ZO" , "MEU"]
    resultat = ""
    resultat = shadok[n%4]
    n = n // 4
    while n !=0 :
        resultat = shadok[n%4] + " " + resultat
        n = n // 4
    return (resultat)