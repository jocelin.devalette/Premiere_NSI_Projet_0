def conv(chaine) :
    """ Prend en paramètre un chaine de caractère
    renvoie la valeur shadok corespondante
    renvoie None si la chaine ne correspond pas"""
    if chaine == 'GA' :
        return 0
    elif chaine == 'BU' :
        return 1
    elif chaine == 'ZO' :
        return 2
    elif chaine == "MEU" :
        return 3
    else :
        return None