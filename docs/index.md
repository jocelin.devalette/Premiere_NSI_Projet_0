# Première NSI - Projet 0

!!! danger "Objectif de l'activité"
    L’objectif de cette activité est d’aider les shadoks à cosntruire leur première calculatrice. Une calculatrice qui fait des additions des soustractions et même des multiplications.
    Certes en Python il est facile de faire des calculs ... mais en base 10, et les shadoks ne calculent pas en base 10... 

## Et les Shadoks comptaient

!!! info "Comptons comme les Shadoks"

    Essayons dans un premier temps de comprendre comment comptent les shadoks. Pour cela visionnons le petit film ci-dessous :

    <iframe width="560" height="315" src="https://www.youtube.com/embed/lP9PaDs2xgQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    Répondez aux questions ci-dessous pour vérifier si vous avez bien compris comment comptent les Shadoks.

    !!! question "Question 1"

        === "Question"
            Donner en décimal la valeur de **BU BU**

        === "Solution"
            5

    !!! question "Question 2"
        === "Question"
            Comment s'écrit en shadok le chiffre 9

        === "Solution"
            **ZO BU**



    
    



