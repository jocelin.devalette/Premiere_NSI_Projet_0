# Passer du décimal au Shadok

## Convertir de décimal en Shadok

!!! info "Algorithme de conversion"

    Le schéma ci-dessous représente la conversion de `57` en `MEU ZO BU`

    ```mermaid
    flowchart LR
        57 -- /4 --> 14 
        57 -- reste --> 1  
        14 -- /4 --> 3
        14 -- reste --> 2
        3 -- /4 --> 0
        3 -- reste --> 3.
        1 -- shadok --> BU
        2 -- shadok --> ZO
        3. -- shadok --> MEU
        MEU ==> ZO
        ZO ==> BU
    ```

!!! tip "Fonction de conversion de décimal en Shadok"
    ??? note "Compléter le script"
        On rapelle que en Python :

        - `//` représente le résultat de la division euclidienne  ( par exemple : `9//4 = 2`)
        - `%` représente le reste de la division euclidienne ( par exemple : ` 9%4 = 1`)

        {{IDE('exo3/dectoshadock')}}