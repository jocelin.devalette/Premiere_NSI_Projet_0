# Passer du Shadok au décimal

## Objectif 

!!! danger "Objectif de cette première page"
    L'objectif de cette première page est de convertir n'importe quel nombre exprimé en Shadok en un entier décimal.

## Convertir de Shadok en décimal

!!! tip "Convertir les unités Shadok"
    Ecrire une fonction `conv(chaine)` qui prend en argument une chaine de caractéres ’GA’ , ’BU’ , ’ZO’ ou ’MEU’  et qui renvoie la valeur correspondante 0,1,2 ou 3. La fonction renvoie `None`, si la chaine ne correspond pas à un des 4 chiffres Shadok.

    ??? note "Compléter le script"
        {{IDE('conv')}}

!!! tip "Fonction de conversion de Shadok en décimal"
    Essayons de convertir $MEU~ZO~BU$ en notation décimale :

    - On peut écrire $MEU~ZO~BU$ en notation chiffrée : $3~2~1$.
    - Puis calculer $3 \cdot 4^2 + 2 \cdot 4^1 + 1 \cdot 4^0 = 3 \cdot 16 + 2 \cdot 4 + 1 \cdot 1 = 57$

    **Ecrire une fonction `shadoktodec(chaine)` qui prend en paramètre une chaine de caractère représentant un nombre écrit en Shadok et renvoie le nombre correspondant en décimal.**

    Pour cela vous utiliserez :

    - La fonction `conv(chaine)` précédemment codée. (attention de bien l'avoir éxécuter avant de passer à cette partie)
    - La méthode `.split()` (Cf. ci dessous)

    ??? info "la méthode `split`"
        `split()` permet de diviser une chaîne de caractères à partir d'un séparateur passé en argument.

        Exemple :
        ```python
            >>>"MEU ZO BU".split(" ")
            ["MEU", "ZO", "BU"]
        ```

    ??? note "Complétez le script"
        {{IDE('shadoktodec')}}